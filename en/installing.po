# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-07-05 15:31+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr ""

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr ""

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian "
"website</ulink>."
msgstr ""

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr ""

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink "
"url=\"&url-installer;index#errata\">errata</ulink> for debian-installer for "
"a list of known issues."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"Most notably there is the initial support for UEFI Secure Boot (see <xref "
"linkend=\"secure-boot\"/>), which has been added to the installation images."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:45
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:124
msgid "Automated installation"
msgstr ""

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:126
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:133
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
