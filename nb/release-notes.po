# translation of release-notes.po to Norwegian bokmål
# SOME DESCRIPTIVE TITLE
# Copyright (C) 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Klaus Ade Johnstad <klaus@skolelinux.no>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: release-notes\n"
"POT-Creation-Date: 2017-01-26 23:48+0100\n"
"PO-Revision-Date: 2009-03-29 12:00+0200\n"
"Last-Translator: Klaus Ade Johnstad <klaus@skolelinux.no>\n"
"Language-Team: Norwegian bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. type: Attribute 'lang' of: <book>
#: en/release-notes.dbk:8
msgid "en"
msgstr "nb"

#. type: Content of: <book><title>
#: en/release-notes.dbk:9
msgid "Release Notes for &debian; &release; (&releasename;), &arch-title;"
msgstr "Utgivelsesnotater for &debian; &release; (&releasename;), &arch-title;"

#. type: Content of: <book><subtitle>
#: en/release-notes.dbk:11
msgid ""
"<ulink url=\"http://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"
msgstr ""
"<ulink url=\"http://www.debian.org/doc/\">The Debian Documentation Project</"
"ulink>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:17
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Steve</firstname> <surname>Langasek</"
#| "surname>"
msgid ""
"<firstname>Steve</firstname> <surname>Langasek</surname> "
"<email>vorlon@debian.org</email>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:22
#, fuzzy
#| msgid ""
#| "<editor> <firstname>W. Martin</firstname> <surname>Borgert</surname> "
#| "<email>debacle@debian.org</email> </editor> <editor condition=\"fixme\"> "
#| "<firstname></firstname> <surname></surname>"
msgid ""
"<firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email>"
msgstr ""
"<editor> <firstname>W. Martin</firstname> <surname>Borgert</surname> "
"<email>debacle@debian.org</email> </editor> <editor condition=\"fixme\"> "
"<firstname></firstname> <surname></surname>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:27
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
#| "Sanguino Peña</surname>"
msgid ""
"<firstname>Javier</firstname> <surname>Fernandez-Sanguino</surname> "
"<email>jfs@debian.org</email>"
msgstr ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:32
msgid ""
"<firstname>Julien</firstname> <surname>Cristau</surname> "
"<email>jcristau@debian.org</email>"
msgstr ""

#. type: Content of: <book><bookinfo><editor>
#: en/release-notes.dbk:37
#, fuzzy
#| msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgid "<firstname></firstname> <surname></surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><bookinfo><editor><contrib>
#: en/release-notes.dbk:39
msgid "There were more people!"
msgstr "Det var flere!"

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:43
msgid ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."
msgstr ""
"This document is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License, version 2, as published "
"by the Free Software Foundation."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:49
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:56
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."
msgstr ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."

#. type: Content of: <book><bookinfo><legalnotice><para>
#: en/release-notes.dbk:61
#, fuzzy
#| msgid ""
#| "The license text can also be found at <ulink url=\"http://www.gnu.org/"
#| "copyleft/gpl.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
#| "filename> on &debian;."
msgid ""
"The license text can also be found at <ulink url=\"http://www.gnu.org/"
"licenses/gpl-2.0.html\"/> and <filename>/usr/share/common-licenses/GPL-2</"
"filename> on &debian;."
msgstr ""
"Lisensteksten kan også hentes fra <ulink url=\"http://www.gnu.org/copyleft/"
"gpl.html\"/> og <filename>/usr/share/common-licenses/GPL-2</filename> på "
"ditt  &debian; system."

#. type: Content of: <book><appendix><title>
#: en/release-notes.dbk:84
msgid "Contributors to the Release Notes"
msgstr "Bidragsytere til Utgivelsenotatene"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:86
msgid ""
"Many people helped with the release notes, including, but not limited to"
msgstr "Mange har hjulpet med å skrive dette Utgivelsenotat, blant andre"

#.  alphabetical (LANG=C sort) order by firstname 
#.  the contrib will not be printed, but is a reminder for the editor;
#.          username as shown in svn log, contribution  
#.  list of translators will only show up in translated texts, only list
#.          contributors to en/ here 
#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:94
msgid "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"
msgstr "<author> <firstname>Adam</firstname> <surname>Di Carlo</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:97 en/release-notes.dbk:197
#, fuzzy
#| msgid "previous release"
msgid "previous releases"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:98
msgid ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"
msgstr ""
"</author>, <author> <firstname>Andreas</firstname> <surname>Barth</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:102
#, fuzzy
#| msgid "previous release"
msgid "aba, previous releases: 2005 - 2007"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:103
msgid ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"
msgstr ""
"</author>, <author> <firstname>Andrei</firstname> <surname>Popescu</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:107 en/release-notes.dbk:142 en/release-notes.dbk:232
#: en/release-notes.dbk:252
msgid "various contributions"
msgstr "flere bidrag"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:108
msgid ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"
msgstr ""
"</author>, <author> <firstname>Anne</firstname> <surname>Bezemer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:112 en/release-notes.dbk:117
msgid "previous release"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:113
msgid ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"
msgstr ""
"</author>, <author> <firstname>Bob</firstname> <surname>Hilliard</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:118
msgid ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"
msgstr ""
"</author>, <author> <firstname>Charles</firstname> <surname>Plessy</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:122
msgid "description of GM965 issue"
msgstr "beskrivelse av feilen med GM965 "

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:123
msgid ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Christian</firstname> <surname>Perrier</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:127
#, fuzzy
#| msgid "Lenny installation"
msgid "bubulle, Lenny installation"
msgstr "For Lenny"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:128
msgid ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"
msgstr ""
"</author>, <author> <firstname>Daniel</firstname> <surname>Baumann</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:132
msgid "Debian Live"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:133
#, fuzzy
#| msgid ""
#| "</author> <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgid ""
"</author>, <author> <firstname>David</firstname> <surname>Prévot</surname>"
msgstr "</author> <author> <firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:137
msgid "taffit, Wheezy release"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:138
msgid ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"
msgstr ""
"</author>, <author> <firstname>Eddy</firstname> <surname>Petrișor</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:143
msgid ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"
msgstr ""
"</author>, <author> <firstname>Emmanuel</firstname> <surname>Kasper</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:147
msgid "backports"
msgstr "backports"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:148
msgid ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"
msgstr ""
"</author>, <author> <firstname>Esko</firstname> <surname>Arajärvi</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:152
msgid "rework X11 upgrade"
msgstr "omskriving av X11 oppgradering"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:153
#, fuzzy
#| msgid ""
#| "</author> <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgid "</author>, <author> <firstname>Frans</firstname> <surname>Pop</surname>"
msgstr "</author> <author> <firstname>Frans</firstname> <surname>Pop</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:157
#, fuzzy
#| msgid "previous release"
msgid "fjp, previous release (Etch)"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:158
msgid ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Giovanni</firstname> <surname>Rapagnani</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:162 en/release-notes.dbk:242
msgid "innumerable contributions"
msgstr "fler bidrag"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:163
msgid ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Gordon</firstname> <surname>Farquharson</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:167 en/release-notes.dbk:222
msgid "ARM port issues"
msgstr "Problemer med ARM porten"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:168
msgid ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"
msgstr ""
"</author>, <author> <firstname>Javier</firstname> <surname>Fernández-"
"Sanguino Peña</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:172
#, fuzzy
#| msgid "previous release"
msgid "jfs, Etch release, Squeeze release"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:173
msgid ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgstr ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:177
msgid "German translation, innumerable contributions"
msgstr "Tysk oversettelse, flere bidrag"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:178
msgid ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgstr ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:182 en/release-notes.dbk:227
msgid "syslog issues"
msgstr "Problemer med syslog"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:183
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"
msgid ""
"</author>, <author> <firstname>Jonathan</firstname> <surname>Nieder</surname>"
msgstr ""
"</author>, <author> <firstname>Jonas</firstname> <surname>Meurer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:187
#, fuzzy
#| msgid "previous release"
msgid "jrnieder@gmail.com, Squeeze release, Wheezy release"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:188
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgid ""
"</author>, <author> <firstname>Joost</firstname> <surname>van Baal-Ilić</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:192
msgid "joostvb, Wheezy release, Jessie release"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:193
msgid ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"
msgstr ""
"</author>, <author> <firstname>Josip</firstname> <surname>Rodin</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:198
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"
msgid ""
"</author>, <author> <firstname>Julien</firstname> <surname>Cristau</surname>"
msgstr ""
"</author>, <author> <firstname>Jens</firstname> <surname>Seidel</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:202
#, fuzzy
#| msgid "previous release"
msgid "jcristau, Squeeze release, Wheezy release"
msgstr "forrige utgivelse"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:203
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Justin B</firstname> <surname> Rye</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Justin B</firstname> <surname>Rye</surname>"
msgstr ""
"</author>, <author> <firstname>Justin B</firstname> <surname> Rye</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:207
msgid "English fixes"
msgstr "Korrigeringer i den engelske orginalteksten"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:208
msgid ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"
msgstr ""
"</author>, <author> <firstname>LaMont</firstname> <surname>Jones</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:212
msgid "description of NFS issues"
msgstr "beskrivelse av NFS problemer"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:213
msgid "</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"
msgstr ""
"</author>, <author> <firstname>Luk</firstname> <surname>Claes</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:217
msgid "editors motivation manager"
msgstr "ansvarlig for å oppmuntre andre redaktører"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:218
msgid ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Martin</firstname> <surname>Michlmayr</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:223
msgid ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"
msgstr ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:228
msgid ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Moritz</firstname> <surname>Mühlenhoff</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:233
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Michael</firstname> <surname>Biebl</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Niels</firstname> <surname>Thykier</surname>"
msgstr ""
"</author>, <author> <firstname>Michael</firstname> <surname>Biebl</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:237
msgid "nthykier, Jessie release"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:238
msgid ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"
msgstr ""
"</author>, <author> <firstname>Noah</firstname> <surname>Meyerhans</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:243
msgid ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Noritada</firstname> <surname>Kobayashi</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:247
msgid "Japanese translation (coordination), innumerable contributions"
msgstr "Japansk oversettelse (koordinering), flere bidrag"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:248
msgid ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"
msgstr ""
"</author>, <author> <firstname>Osamu</firstname> <surname>Aoki</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:253
msgid ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"
msgstr ""
"</author>, <author> <firstname>Peter</firstname> <surname>Green</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:257
msgid "kernel version note"
msgstr "om kjernen"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:258
msgid ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"
msgstr ""
"</author>, <author> <firstname>Rob</firstname> <surname>Bradford</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:262 en/release-notes.dbk:287
msgid "Etch release"
msgstr ""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:263
msgid ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"
msgstr ""
"</author>, <author> <firstname>Samuel</firstname> <surname>Thibault</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:267 en/release-notes.dbk:272
msgid "description of d-i Braille support"
msgstr "beskrivelse av Debian Installer sin støtte for leselist"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:268
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Bienlein</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:273
msgid ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"
msgstr ""
"</author>, <author> <firstname>Simon</firstname> <surname>Paillard</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:277
#, fuzzy
#| msgid "innumerable contributions"
msgid "spaillar-guest, innumerable contributions"
msgstr "fler bidrag"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:278
msgid ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"
msgstr ""
"</author>, <author> <firstname>Stefan</firstname> <surname>Fritsch</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:282
msgid "description of Apache issues"
msgstr "beskrivelse av Apache problemer"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:283
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"
msgstr ""
"</author>, <author> <firstname>Steve</firstname> <surname>Langasek</surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:288
#, fuzzy
#| msgid ""
#| "</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
#| "surname>"
msgid ""
"</author>, <author> <firstname>Steve</firstname> <surname>McIntyre</surname>"
msgstr ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:292
#, fuzzy
#| msgid "Debian Live"
msgid "Debian CDs"
msgstr "Debian Live"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:293
msgid ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"
msgstr ""
"</author>, <author> <firstname>Tobias</firstname> <surname>Scherer</surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:297 en/release-notes.dbk:302
msgid "description of \"proposed-update\""
msgstr "beskrivelse av \"proposed-update\""

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:298
msgid ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"
msgstr ""
"</author>, <author> <firstname>Vincent</firstname> <surname>McIntyre</"
"surname>"

#. type: Content of: <book><appendix><para><author>
#: en/release-notes.dbk:303
msgid ""
"</author>, and <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"
msgstr ""
"</author>, og <author> <firstname>W. Martin</firstname> <surname>Borgert</"
"surname>"

#. type: Content of: <book><appendix><para><author><contrib>
#: en/release-notes.dbk:308
msgid "editing Lenny release, switch to DocBook XML"
msgstr "korrigering for Lenny utgaven, overgang til DocBook XML"

#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:309
msgid "</author>."
msgstr "</author>."

#.  translator names here, depending on language!
#.     </para>
#. <para>Translated into Klingon by:
#.     <author>
#.       <firstname>Firstname1</firstname>
#.       <surname>Surname1</surname>
#.       <contrib>Foo translation</contrib>
#.     </author>,
#.     <author>
#.       <firstname>Firstname2</firstname>
#.       <surname>Surname2</surname>
#.       <contrib>Foo translation</contrib>
#.     </author 
#. type: Content of: <book><appendix><para>
#: en/release-notes.dbk:312
msgid ""
"This document has been translated into many languages.  Many thanks to the "
"translators!"
msgstr ""
"Dette dokument har blitt oversatt til flere språk. Mange takk til "
"oversetterene.\n"
"    </para><para>Norsk bokmål oversettelse:    <author>      "
"<firstname>Klaus Ade</firstname>      <surname>Johnstad</surname></author>"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:335
msgid "ACPI"
msgstr "ACPI"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:336
msgid "Advanced Configuration and Power Interface"
msgstr "Advanced Configuration and Power Interface"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:339
msgid "ALSA"
msgstr "ALSA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:340
msgid "Advanced Linux Sound Architecture"
msgstr "Advanced Linux Sound Architecture"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:343
msgid "APM"
msgstr "APM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:344
msgid "Advanced Power Management"
msgstr "Advanced Power Management"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:347
msgid "BD"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:348
msgid "Blu-ray Disc"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:351
msgid "CD"
msgstr "CD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:352
msgid "Compact Disc"
msgstr "Compact Disc"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:355
msgid "CD-ROM"
msgstr "CD-ROM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:356
msgid "Compact Disc Read Only Memory"
msgstr "Compact Disc Read Only Memory"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:359
msgid "DHCP"
msgstr "DHCP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:360
msgid "Dynamic Host Configuration Protocol"
msgstr "Dynamic Host Configuration Protocol"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:363
msgid "DNS"
msgstr "DNS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:364
msgid "Domain Name System"
msgstr "Domain Name System"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:367
msgid "DVD"
msgstr "DVD"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:368
msgid "Digital Versatile Disc"
msgstr "Digital Versatile Disc"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:371
msgid "GIMP"
msgstr "GIMP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:372
msgid "GNU Image Manipulation Program"
msgstr "GNU Image Manipulation Program"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:375
msgid "GNU"
msgstr "GNU"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:376
msgid "GNU's Not Unix"
msgstr "GNU's Not Unix"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:379
msgid "GPG"
msgstr "GPG"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:380
msgid "GNU Privacy Guard"
msgstr "GNU Privacy Guard"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:383
msgid "IDE"
msgstr "IDE"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:384
msgid "Integrated Drive Electronics"
msgstr "Integrated Drive Electronics"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:387
msgid "LDAP"
msgstr "LDAP"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:388
msgid "Lightweight Directory Access Protocol"
msgstr "Lightweight Directory Access Protocol"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:391
msgid "LILO"
msgstr "LILO"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:392
msgid "LInux LOader"
msgstr "LInux LOader"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:395
msgid "LSB"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:396
#, fuzzy
#| msgid "Linux Standards Base"
msgid "Linux Standard Base"
msgstr "Linux Standards Base"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:399
msgid "LVM"
msgstr "LVM"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:400
msgid "Logical Volume Manager"
msgstr "Logical Volume Manager"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:403
msgid "MTA"
msgstr "MTA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:404
msgid "Mail Transport Agent"
msgstr "Mail Transport Agent"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:407
msgid "NBD"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:408
#, fuzzy
#| msgid "Network Information Service"
msgid "Network Block Device"
msgstr "Network Information Service"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:411
msgid "NFS"
msgstr "NFS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:412
msgid "Network File System"
msgstr "Network File System"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:415
msgid "NIC"
msgstr "NIC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:416
msgid "Network Interface Card"
msgstr "Network Interface Card"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:419
msgid "NIS"
msgstr "NIS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:420
msgid "Network Information Service"
msgstr "Network Information Service"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:423
msgid "OSS"
msgstr "OSS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:424
msgid "Open Sound System"
msgstr "Open Sound System"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:427
msgid "RAID"
msgstr "RAID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:428
msgid "Redundant Array of Independent Disks"
msgstr "Redundant Array of Independent Disks"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:431
msgid "RPC"
msgstr "RPC"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:432
msgid "Remote Procedure Call"
msgstr "Remote Procedure Call"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:435
msgid "SATA"
msgstr "SATA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:436
msgid "Serial Advanced Technology Attachment"
msgstr "Serial Advanced Technology Attachment"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:439
#, fuzzy
#| msgid "OSS"
msgid "SSL"
msgstr "OSS"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:440
msgid "Secure Sockets Layer"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:443
#, fuzzy
#| msgid "LSB"
msgid "TLS"
msgstr "LSB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:444
msgid "Transport Layer Security"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:447
msgid "UEFI"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:448
msgid "Unified Extensible Firmware Interface"
msgstr ""

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:451
msgid "USB"
msgstr "USB"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:452
msgid "Universal Serial Bus"
msgstr "Universal Serial Bus"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:455
msgid "UUID"
msgstr "UUID"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:456
msgid "Universally Unique Identifier"
msgstr "Universally Unique Identifier"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:459
msgid "VGA"
msgstr "VGA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:460
msgid "Video Graphics Array"
msgstr "Video Graphics Array"

#. type: Content of: <book><glossary><glossentry><glossterm>
#: en/release-notes.dbk:463
msgid "WPA"
msgstr "WPA"

#. type: Content of: <book><glossary><glossentry><glossdef><para>
#: en/release-notes.dbk:464
msgid "Wi-Fi Protected Access"
msgstr "Wi-Fi Protected Access"

#, fuzzy
#~| msgid "previous release"
#~ msgid "previous release (Etch)"
#~ msgstr "forrige utgivelse"

#~ msgid "</editor>"
#~ msgstr "</editor>"

#, fuzzy
#~| msgid "2009-02-14"
#~ msgid "2010-11-12"
#~ msgstr "2009-02-14"

#~ msgid "Lenny dedicated to Thiemo Seufer"
#~ msgstr "Til minne om Thiemo Seufer"

#~ msgid ""
#~ "The Debian Project has lost an active member of its community. Thiemo "
#~ "Seufer died on December 26th, 2008 in a tragic car accident."
#~ msgstr ""
#~ "Debianprosjektet har mistet et viktig medlem. Thiemo Seufer, døde i en "
#~ "bilulykke 26. desember 2008."

#~ msgid ""
#~ "Thiemo was involved in Debian in many ways. He maintained several "
#~ "packages and was the main supporter of the Debian ports to the MIPS "
#~ "architecture. He was also a member of our kernel team, as well as a "
#~ "member of the Debian Installer team. His contributions reached far beyond "
#~ "the Debian project: he also worked on the MIPS port of the Linux kernel, "
#~ "the MIPS emulation of qemu, and far too many smaller projects to be named "
#~ "here."
#~ msgstr ""
#~ "Thiemo var involvert i Debian på mange måter, han vedlikeholdte flere "
#~ "pakker, og var hovedsponsoren for porteringen av Debian til MIPS "
#~ "prosessorvarianter. Han var også medlem av linuxkjerne-laget til Debian, "
#~ "samt medlem av Debian Installer laget. Hans bidrag strak seg lengre enn "
#~ "til bare Debian prosjektet. Han jobbet også med en portering av Linux "
#~ "kjernen til MIPS, emuleringen av MIPS i qemu, samt mange andre mindre "
#~ "prosjekter."

#~ msgid ""
#~ "Thiemo's work, dedication, broad technical knowledge and ability to share "
#~ "this with others will be missed. His contributions will not be "
#~ "forgotten.  The high standards of Thiemo's work make it hard to pick up."
#~ msgstr ""
#~ "Thiemos arbeid, hans hengivelse, hans brede tekniske innsikt og hans evne "
#~ "til å dele dette med andre vil bli sårt savnet. Thiemos bidrag vil ikke "
#~ "bli glemt. Den høye standarden på hans arbeid gjør det hard å fortsette "
#~ "dette arbeidet."

#~ msgid ""
#~ "To honour his contributions to Debian, the project dedicates the release "
#~ "of Debian GNU/Linux 5.0 <quote>Lenny</quote> to Thiemo."
#~ msgstr ""
#~ "For å hedre Thiemos bidrag til Debian dedikeres denne utgaven  av Debian "
#~ "GNU/Linux 5.0 <quote>&releasename;</quote> til han."
